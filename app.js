

$('#builder').queryBuilder({
    filters: [
    {
		id: 'name',
		label: 'Name',
		type: 'string',
		default_value: '',
		size: 30,
		unique: true
	}, 
	{
		id: 'adicao',
		label: 'Adição',
		type: 'string',
		default_value: '',
		size: 30,
		unique: true
	},
	{
		id: 'fornecedor',
		label: 'Fornecedor',
		type: 'string',
		default_value: '',
		size: 30,
		unique: true
	},
	{
		id: 'liberacao',
		label: 'Liberação de Importação',
		type: 'string',
		default_value: '',
		size: 30,
		unique: true
	},
	{
		id: 'cod',
		label: 'Codigo do Item',
		type: 'string',
		default_value: '',
		size: 30,
		unique: true
	},
	{
		id: 'purch',
		label: 'Purchase Order',
		type: 'string',
		default_value: '',
		size: 30,
		unique: true
	},
	{
		id: 'po',
		label: 'PO',
		type: 'string',
		default_value: '',
		size: 30,
		unique: true
	},


		]
  });


$('.parse-json').on('click', function(){
	console.log(JSON.stringify(
		$('#builder').queryBuilder('getRules'),
		undefined, 2	
		));
})